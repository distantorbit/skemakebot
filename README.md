# SkeMakeBot

A helper application for managing resources available to your Makerspace. Originally developed for SkeMake in Skellefteå, Sweden.

## Implemented Features

null

## Planned Features

### Resource scheduler

* Register resources (3D printers, lasers, CNC machines, etc.)
* Schedule resource availability (date, time, cost)
* Queue/priority system that tracks time spent on resources

### Project database

* Upload dxf, stl, obj files, with pictures and descriptions
* Gallery
* Search

### Slack integration
* List available commands
* Search available resources
* Request time on a resource
* Login from Slack

## License

This is is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
